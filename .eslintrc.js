const config = require('@fathom/project-config');

module.exports = {
  root: true,
  overrides: [
    {
      files: ['*.js'],
      parser: 'babel-eslint',
      rules: {
        'prettier/prettier': [
          'error',
          {
            ...config.prettier,
          },
        ],
      },
    },
    {
      files: ['*.ts'],
      parserOptions: {
        project: ['tsconfig.json'],
        createDefaultProgram: true,
      },
      ...config.eslint,
    },
    {
      files: ['*.spec.ts'],
      rules: {
        '@typescript-eslint/typedef': 'off',
      },
    },
    {
      files: ['*.html'],
      extends: ['plugin:@angular-eslint/template/recommended'],
      rules: {},
    },
  ],
};
