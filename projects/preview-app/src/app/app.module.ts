import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ProfileModule } from '../../../../projects/profile-library/src/public-api';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, ProfileModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
