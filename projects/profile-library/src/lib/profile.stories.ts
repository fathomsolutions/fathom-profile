import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { Story, Meta } from '@storybook/angular/types-6-0';

import { ProfileComponent } from './profile.component';

export default {
  title: 'Profile',
  component: ProfileComponent,
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [CommonModule],
    }),
  ],
  argTypes: {},
} as Meta;

const template: Story<ProfileComponent> = (args: ProfileComponent) => ({
  props: args,
});

export const base = template.bind({});
base.args = {};
