module.exports = {
  stories: ['../projects/profile-library/**/*.stories.mdx', '../projects/profile-library/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials', '@storybook/addon-notes/register'],
  core: {
    builder: 'webpack5',
  },
};
